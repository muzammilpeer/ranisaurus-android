package com.ranisaurus.mobileapplication.fragment;

import android.app.SearchManager;
import android.content.ComponentName;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.ranisaurus.baselayer.adapter.GeneralBaseAdapter;
import com.ranisaurus.baselayer.fragment.BaseFragment;
import com.ranisaurus.databaselayer.factory.ServiceFactory;
import com.ranisaurus.databaselayer.model.DBCategories;
import com.ranisaurus.databaselayer.service.ICategoriesService;
import com.ranisaurus.mobileapplication.R;
import com.ranisaurus.mobileapplication.activity.MainActivity;
import com.ranisaurus.mobileapplication.cell.CategoryCell;
import com.ranisaurus.newtorklayer.enums.NetworkRequestEnum;
import com.ranisaurus.newtorklayer.manager.NetworkManager;
import com.ranisaurus.newtorklayer.models.Categories;
import com.ranisaurus.newtorklayer.models.CategoriesRequestModel;
import com.ranisaurus.newtorklayer.models.CategoriesResponseModel;
import com.ranisaurus.newtorklayer.requests.ListCategoriesRequest;
import com.ranisaurus.utilitylayer.keyboard.KeyboardUtil;
import com.ranisaurus.utilitylayer.logger.Log4a;
import com.ranisaurus.utilitylayer.network.GsonUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainFragment extends BaseFragment implements SearchView.OnQueryTextListener, View.OnClickListener {

    @Bind(R.id.srl_categories)
    SwipeRefreshLayout categoriesSwipeRefreshLayout;

    @Bind(R.id.rv_categories)
    RecyclerView categoriesRecyclerView;

    @Bind(R.id.fab_add_tagline)
    FloatingActionButton addTagLineFloatingActionButton;

    GeneralBaseAdapter<CategoryCell> categoryAdapter;

    public MainFragment() {
    }

    public static BaseFragment createInstance()
    {
        MainFragment fragment = new MainFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            // Associate searchable configuration with the SearchView
            SearchManager searchManager = (SearchManager) mContext.getSystemService(Context.SEARCH_SERVICE);
            SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));

            searchView.setQueryHint(getString(R.string.title_search));
            EditText editSearch = (EditText) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
            editSearch.setHintTextColor(Color.WHITE);
            editSearch.setTextColor(Color.GRAY);

            if (searchView != null) {
                searchView.setSearchableInfo(searchManager.getSearchableInfo(
                        new ComponentName(mContext, MainActivity.class)));
                searchView.setOnQueryTextListener(this);
            }
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        Log4a.e("onQueryTextSubmit", query);
        searchInLocalDatabase(query);
        KeyboardUtil.hideKeyboard(getBaseActivity());
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        Log4a.e("onQueryTextChange", newText);
        searchInLocalDatabase(newText);
        return false;
    }

    //search in database
    private void searchInLocalDatabase(String creteria) {
        try {
            ICategoriesService service =
                    (ICategoriesService) ServiceFactory.getInstance().getService(ICategoriesService.class.getName());

            if (creteria != null && creteria.length() > 0) {

                this.getLocalDataSource().clear();
                this.getLocalDataSource().addAll(service.searchQuery(creteria));
                if (categoryAdapter != null) {
                    categoryAdapter.notifyDataSetChanged();
                }
            } else {
                refreshCategoriesListFromDatabase();
            }

        } catch (Exception e) {
            Log4a.printException(e);
        }
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, R.layout.fragment_main);

        return mView;
    }

    @Override
    public void initViews() {
        super.initViews();

        getBaseActivity().hideBackButton();
        getBaseActivity().setScreenTitle(R.string.title_categories);
        getBaseActivity().restoreToolBarColorWithStatusBar();
    }

    @Override
    public void initObjects() {
        super.initObjects();

    }

    @Override
    public void initListenerOrAdapter() {
        super.initListenerOrAdapter();

        final LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
        categoriesRecyclerView.setLayoutManager(layoutManager);
        categoryAdapter = new GeneralBaseAdapter<CategoryCell>(mContext,R.layout.row_category,CategoryCell.class,this.getLocalDataSource());
        categoriesRecyclerView.setAdapter(categoryAdapter);


        categoriesSwipeRefreshLayout.setColorSchemeResources(R.color.icon_color_0, R.color.icon_color_8, R.color.icon_color_5);
        categoriesSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        getListData();
                        showHideSwipyRefresh(true);
                    }
                }, 2500);
            }
        });


        addTagLineFloatingActionButton.setOnClickListener(this);

    }

    @Override
    public void initNetworkCalls() {
        super.initNetworkCalls();

        //test multipart form

//        LoginRequestModel model  = new LoginRequestModel();
//        model.setAction("login");
//        model.setEmail("gitex@gmail.com");
//        model.setPassword("123456");
//
//        LoginRequest request = new LoginRequest(model);
//        try {
//            NetworkManager.getInstance().executeRequest(request,this,NetworkRequestEnum.LOGIN_REQUEST);
//
//        }catch (Exception e)
//        {
//            Log4a.printException(e);
//        }

        try {
            ICategoriesService service =
                    (ICategoriesService) ServiceFactory.getInstance().getService(ICategoriesService.class.getName());

            if (service.getCategoriesCount() == 0)
            {
                showLoader();
                getListData();
            }else
            {
                refreshCategoriesListFromDatabase();
            }

        }catch (Exception e)
        {
            Log4a.printException(e);
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.fab_add_tagline :
            {
                TagLineDetailFragment addTagLine = new TagLineDetailFragment();
                getBaseActivity().replaceFragment(addTagLine,R.id.container_main);

            }break;
            default:break;
        }
    }


    private void refreshCategoriesListFromDatabase()
    {
        try {
            ICategoriesService service =
                    (ICategoriesService) ServiceFactory.getInstance().getService(ICategoriesService.class.getName());
            this.getLocalDataSource().clear();
            this.getLocalDataSource().addAll(service.getAllCategories());
            showHideSwipyRefresh(false);
            if (categoryAdapter != null)
            {
                categoryAdapter.notifyDataSetChanged();
            }
        }catch (Exception e)
        {
            Log4a.printException(e);
        }
    }

    //
    private void showHideSwipyRefresh(boolean isShow)
    {
        if (categoriesSwipeRefreshLayout != null )
        {
            categoriesSwipeRefreshLayout.setRefreshing(isShow);
        }
    }

    //Network Requests
    private void getListData() {
        CategoriesRequestModel model = new CategoriesRequestModel();
        model.setAction("getcategories");
        ListCategoriesRequest request = new ListCategoriesRequest(model);

        try {
            NetworkManager.getInstance().executeRequest(request, this,
                    NetworkRequestEnum.CATEGORIES_LIST);
        } catch (Exception e) {
            Log4a.printException(e);
        }
    }
    // listener


    @Override
    public void responseWithError(Exception error, NetworkRequestEnum requestType) {
        super.responseWithError(error, requestType);
        try {
            if (mView != null) {
                switch (requestType) {
                    case CATEGORIES_LIST: {
                        showHideSwipyRefresh(false);
                    }
                }
            }
        } catch (Exception e)
        {
            Log4a.printException(e);
        }
    }



    @Override
    public void successWithData(Object data, NetworkRequestEnum requestType) {
        super.successWithData(data, requestType);
        try {
            if (mView != null) {
                switch (requestType) {
                    case CATEGORIES_LIST: {
                        CategoriesResponseModel model = (CategoriesResponseModel) GsonUtil.getObjectFromJsonObject(data, CategoriesResponseModel.class);

                        if (model != null) {
                            if (model.getCategories().size() > 0 ) {
                                try {
                                    ICategoriesService service =
                                            (ICategoriesService) ServiceFactory.getInstance().getService(ICategoriesService.class.getName());
                                    List<DBCategories> responseList =  new ArrayList<DBCategories>();
                                    for (Categories dataModel : model.getCategories())
                                    {
                                        responseList.add(new DBCategories(dataModel));
                                    }
                                    service.saveAllCategories(responseList);
                                }catch (Exception e)
                                {
                                    Log4a.printException(e);
                                }
                                //refresh the list from database
                                refreshCategoriesListFromDatabase();
                            }


                        }
                    }
                    break;
                    case LOGIN_REQUEST: {
//                        LoginResponseModel model = (LoginResponseModel) GsonUtil.getObjectFromJsonObject(data, LoginResponseModel.class);
//                        Log4a.e("Response Peer=", model.toString());
                    }
                    break;

                }
            }
        }catch (Exception e)
        {
            Log4a.printException(e);
        }
    }
}
