package com.ranisaurus.mobileapplication.activity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.ranisaurus.baselayer.activity.BaseActivity;
import com.ranisaurus.mobileapplication.R;
import com.ranisaurus.mobileapplication.fragment.MainFragment;
import com.ranisaurus.utilitylayer.view.ResourceUtil;
import com.ranisaurus.utilitylayer.view.ToolBarUtil;

public class MainActivity extends BaseActivity {

    Toolbar mainToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //setup toolbar
        mainToolbar = (Toolbar) findViewById(R.id.tb_main_toolbar);
        setSupportActionBar(mainToolbar);
        mainToolbar.setTitleTextColor(0xFFFFFFFF);

        getSupportActionBar().setHomeAsUpIndicator(whiltetheDrawable(getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha)));


        setupActivity();

        //setup first screen
        MainFragment fragment = new MainFragment();
        addFragment(fragment, R.id.container_main);


    }

    @Override
    public void initViews() {
        super.initViews();

//        setSupportActionBar(mainToolbar);
    }

    @Override
    public void initListenerOrAdapter() {
        super.initListenerOrAdapter();


        mainToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void initObjects() {
        super.initObjects();
    }

    @Override
    public void changeToolBarColorWithStatusBar(int color) {
        super.changeToolBarColorWithStatusBar(color);
        mainToolbar.setBackgroundColor(getResources().getColor(ResourceUtil.getCircularColorResourceID(this, color, getPackageName())));
        ToolBarUtil.changeStatusBarColor(getWindow(), getResources().getColor(ResourceUtil.getStatusBarColorResourceID(this, color, getPackageName())));
    }

    @Override
    public void restoreToolBarColorWithStatusBar() {
        super.restoreToolBarColorWithStatusBar();
        mainToolbar.setBackgroundColor(getResources().getColor(R.color.primary));
        ToolBarUtil.changeStatusBarColor(getWindow(), getResources().getColor(R.color.primary_dark));
    }

    @Override
    public void setScreenTitle(int title) {
        super.setScreenTitle(title);
        getSupportActionBar().setTitle(getString(title));
    }


    @Override
    public void showBackButton() {
        super.showBackButton();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void hideBackButton() {
        super.hideBackButton();
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
//            // Associate searchable configuration with the SearchView
//            SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
//            SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
//
//            if (searchView != null) {
//                searchView.setSearchableInfo(searchManager.getSearchableInfo(
//                        new ComponentName(this, MainActivity.class)));
//                searchView.setOnQueryTextListener(this);
//            }
//        }
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.action_search: {
//                replaceFragment(SearchFragment.createInstance(), R.id.container_main);
            }
            break;
        }
        return super.onOptionsItemSelected(item);
    }


//
//    @Override
//    public boolean onQueryTextSubmit(String query) {
//        Log4a.e("onQueryTextSubmit",query);
//        return false;
//    }
//
//    @Override
//    public boolean onQueryTextChange(String newText) {
//        Log4a.e("onQueryTextChange",newText);
//        return false;
//    }


    private void exitAppDialog() {

        // warning dialog that user is about to exit app
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(R.string.exit_title)
                .setMessage(R.string.exit_message)
                .setPositiveButton(R.string.yes,
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // Stop the activity
                                popAllFragment();
                                finish();
                            }

                        }).setNegativeButton(R.string.no, null).show();
    }

    //on back
    @Override
    public void onBackPressed() {
        if (getFragmentsCount() == 0) {
            exitAppDialog();
        } else {
            super.onBackPressed();
        }
    }
}
