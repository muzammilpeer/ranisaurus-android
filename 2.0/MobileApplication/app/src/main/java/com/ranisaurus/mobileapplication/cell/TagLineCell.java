package com.ranisaurus.mobileapplication.cell;

import android.view.View;
import android.widget.TextView;

import com.ranisaurus.baselayer.cell.BaseCell;
import com.ranisaurus.databaselayer.model.DBTaglines;
import com.ranisaurus.mobileapplication.R;
import com.ranisaurus.mobileapplication.fragment.TagLineDetailFragment;

import butterknife.Bind;

/**
 * Created by muzammilpeer on 8/30/15.
 */
public class TagLineCell extends BaseCell implements View.OnClickListener {
    @Bind(R.id.tv_tagline_row) TextView tvTitle;

    @Bind(R.id.tv_tagline_row_icon) TextView tvIcon;

    public TagLineCell(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);
    }

    @Override
    public void updateCell(Object model) {
        position = this.getAdapterPosition();

        mDataSource = model;
        DBTaglines data = (DBTaglines)mDataSource;

        tvIcon.setText(data.getTaglineIconName());
        //every row has it's on color
//        tvIcon.setBackgroundDrawable(ResourceUtil.getCircularDrawable(itemView, position, getBaseActivity().getPackageName()));
        //define static color for ui
        tvIcon.setBackgroundDrawable(itemView.getResources().getDrawable(R.drawable.icon_circular_main));

        tvTitle.setText(data.getTagline());
    }


    @Override
    public void onClick(View v) {
        //define static color for ui hide the below line
//        getBaseActivity().changeToolBarColorWithStatusBar((int)position);

        getBaseActivity().replaceFragment(TagLineDetailFragment.createInstance(((DBTaglines) mDataSource)), R.id.container_main);
    }

}

